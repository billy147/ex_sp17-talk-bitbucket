[image page](img)
# Creating a Peer tutorial #

This guide explains how to create a peer tutorial for Alkadi's CMPS383 class.
Note that you are not required to do exactly the same things, this is meant as an overview and **NOT** an exact requirement

### How do I start? ###

* Open the email to your repository in bit bucket
* You should see something like this:
>![Main Page](/img/chrome_2017-01-13_13-28-27.png)
* Click the `Create a README` button to make a base file and you should see this:
>![Create ReadME](/img/chrome_2017-01-13_13-28-39.png)
* Then click the blue `Commit` button
* Then click the gray `Commit` button:
>![Commit that stuff](/img/chrome_2017-01-13_16-21-41.png)

### What next? Git Extensions should now be installed ###

I'm glad you asked!
We are going to learn the **basics of Git** in windows so you can start making your guide
* Download [Git Extensions](https://github.com/gitextensions/gitextensions/releases/download/v2.49/GitExtensions-2.49-SetupComplete.msi "Git Ext Version 2.49")
* Go through the setup **BUT** make sure you have these checked:
>![Options to check](/img/msiexec_2017-01-14_13-33-28.png) 
* Otherwise, the defaults provided should be good
* Git Extensions should now be installed

### Prepairing your SSH keys / Bitbucket account ###

* Lets start up Git Extensions:
>![Main window](/img/GitExtensions_2017-01-14_16-59-03.png) 
* Click on `Generate or import key` found here:
>![Generate or import key](/img/GitExtensions_2017-01-14_17-42-55.png) 
* Click on `Generate`
>![Generate](/img/puttygen_2017-01-14_17-45-42.png) 
* After moving your mouse around you'll get something like this:
>![Key sample](/img/puttygen_2017-01-14_17-46-48.png) 
* Change the `Key comment` to something you could recognize (e.g. Home Key 2017-01-14)
* Add a `Key passphrase` to protect your account 
* Copy the long string under `Public key for...`
* The item on your clipboard should look like this:
```
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAip6oKidovucHfzPIgSx0wJtMUYFEmsM9KFuULj4WGdjFfm925iz49ZCLaJ7/E9XgVjWL9iUnh/k9dyGl3Nw1gzhZqAbZKROpQm6WQGFJB27M7Bf+pK6F/MlyfNLTc5M8DlBz3nYngck4xY2uQBWMHP/rqXXhaAITB3oHgzIyvMPv492Yt09e+ZaZVemtnZ7C1A63eBhUAla/4yDxOgJLiP3NlbdjxR0Eh9zFv98mLW39LfMr9PGS2e4f52Am0o47bSyfHJVJgA3FHfZUumGXy/OZEo8Jz9GmSEAXNQfr3lNpSx1HcYh+PTKxRqcEM8hj1+1r71pJDGjO3/mGjkfi/Q== Home Key 2017-01-14
```
* Now click `Save private key` and save it somewhere safe on your computer
* Now [go here](https://bitbucket.org/account/ssh-keys/) to add SSH keys to your bit bucket account
* Hit `Add key` and paste the public key you copied from your clipboard above:
>![Add key](/img/chrome_2017-01-14_17-56-51.png) 
* You should that key added:
>![Key listing](/img/chrome_2017-01-14_17-57-38.png)
* And that is it, with that private key file you saved you can work with your bitbucket account with ssh keys. Other providers like github use a very similar process for authenticating
* We will use that private key later

### How to download a repository ###

* Since you followed the above you will now be able to "clone" your repository
* Go [to the repository listing](https://bitbucket.org/account/user/selucmps383/projects/S2-2) and you should see your group's repository that was provisioned 
* Simply click on the top right `SSH` URI text and copy it:
>![URI](/img/chrome_2017-01-14_18-03-41.png)
* Now go to a folder you want to save your repository to and right click menu to `GitExt Clone...` :
>![Clone](/img/2017-01-14_18-05-10.png)
* Paste the URI (looks like `git@bitbucket.org:selucmps383/sp17-talk-bitbucket.git`) into the `Repository to clone:` box:
>![Clone](/img/GitExtensions_2017-01-14_18-05-57.png)
* Then click the `Clone` button
* You'll get an error and it will ask for your SSH private key:
>![Clone](/img/GitExtensions_2017-01-14_18-07-48.png)
* Click the `Load SSH key` and find that private key file you saved before
* You should be asked for a passphrase for the key, provide that `Key passphrase` you added earlier when generating your SSH key
* It should then download your repository

### How to modify the repository ###

* Once the repository (or repo) is cloned as above you can change the contents as you see fit
* In this case, I made [this *markdown* file](/README.md) using notepad: 
>![Clone](/img/notepad++_2017-01-14_18-13-23.png)
* Talk about inception~
* This is all markdown for this guide. Just follow [this guide](https://bitbucket.org/tutorials/markdowndemo) to see what you can do in markdown 
* Feel free to use [this repository](https://bitbucket.org/selucmps383/sp17-talk-bitbucket) as a guide/base
* Once we have our guide and any any resources for it locally it is time to save it back to bitbucket
* Right click somewhere in the folder you cloned the repo to and click `GitExt Commit...` :
>![Clone](/img/explorer_2017-01-14_18-15-35.png)
* Click the double down arrows ![Double that Down](/img/GitExtensions_2017-01-14_18-16-48.png) in the new window:
* Now provide a reason for the changes in the text box on the bottom right:
>![Clone](/img/GitExtensions_2017-01-14_18-18-40.png)
* Hit the `Commit & push` button
* It should just go, but you may need to provide your SSH private key again
* Your changes should then be visible on bitbucket